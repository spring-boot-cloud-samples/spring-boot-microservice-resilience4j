package in.silentsudo.webapi.config;

import in.silentsudo.webapi.dto.PaymentOption;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = DefaultConfiguration.PAYMENT_SERVICE)
public interface FeignClientPaymentOptions {

    @RequestMapping(method = RequestMethod.GET, value = "/payment")
    List<PaymentOption> getPaymentOptions();

}
