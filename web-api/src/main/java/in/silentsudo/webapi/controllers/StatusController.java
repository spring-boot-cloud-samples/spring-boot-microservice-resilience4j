package in.silentsudo.webapi.controllers;

import in.silentsudo.webapi.services.StoreStatusService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/web-api/status")
public class StatusController {

    private final StoreStatusService storeStatusService;

    @Autowired
    public StatusController(StoreStatusService storeStatusService) {
        this.storeStatusService = storeStatusService;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String webApiStatus() {
        return storeStatusService.getStoreServiceStatus();
    }
}
