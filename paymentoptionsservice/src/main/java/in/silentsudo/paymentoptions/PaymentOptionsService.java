package in.silentsudo.paymentoptions;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class PaymentOptionsService {
    public List<PaymentOption> paymentOptions() throws InterruptedException {
        Thread.sleep(5000);
        return Arrays.asList(
                new PaymentOption("Google Pay", PaymentOption.Type.UPI),
                new PaymentOption("Visa", PaymentOption.Type.CARD)
        );
    }
}
