package in.silentsudo.webapi.services;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class StoreStatusService {
    private final RestTemplate restTemplate;
    private final String storeService;

    @Autowired
    public StoreStatusService(RestTemplate restTemplate, String storeService) {
        this.restTemplate = restTemplate;
        this.storeService = storeService;
    }

    @RateLimiter(name = "store-service-limiter", fallbackMethod = "getStoreServiceRateLimitedFallback")
    public String getStoreServiceStatus() {
        log.info("Store service resolved to {}", storeService);
        return restTemplate.getForObject(storeService + "/status/shh", String.class);
    }

    public String getStoreServiceRateLimitedFallback(Throwable throwable) {
        return throwable.getMessage();
    }
}
