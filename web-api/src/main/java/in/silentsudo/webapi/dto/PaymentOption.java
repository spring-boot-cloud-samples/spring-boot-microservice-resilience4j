package in.silentsudo.webapi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class PaymentOption {
    @Getter
    private String name;

    @Getter
    private Type type;


    public static enum Type {
        UPI,
        CARD,
        COD
    }
}
