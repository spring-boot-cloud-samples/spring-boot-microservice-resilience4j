package in.silentsudo.webapi.controllers;

import in.silentsudo.webapi.config.FeignClientPaymentOptions;
import in.silentsudo.webapi.dto.PaymentOption;
import in.silentsudo.webapi.services.PaymentOptionService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/web-api/payment")
public class PaymentController {

    private final PaymentOptionService paymentOptionService;
    private final FeignClientPaymentOptions feignClientPaymentOptions;

    public PaymentController(PaymentOptionService paymentOptionService, FeignClientPaymentOptions feignClientPaymentOptions) {
        this.paymentOptionService = paymentOptionService;
        this.feignClientPaymentOptions = feignClientPaymentOptions;
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, List<PaymentOption>> getOptions() {
        return this.paymentOptionService.availablePaymentOptions();
    }

    @GetMapping(value = "/feign", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, List<PaymentOption>> getFeignPaymentOptions() {
        final Map<String, List<PaymentOption>> options = new HashMap<>(1);
        options.put("payment_options", feignClientPaymentOptions.getPaymentOptions());
        return options;
    }
}
