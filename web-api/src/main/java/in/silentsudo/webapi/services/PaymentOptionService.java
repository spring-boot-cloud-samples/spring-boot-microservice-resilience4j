package in.silentsudo.webapi.services;

import in.silentsudo.webapi.dto.PaymentOption;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Slf4j
@Service
public class PaymentOptionService {
    private final String paymentService;
    private final RestTemplate restTemplate;

    public PaymentOptionService(String paymentService, RestTemplate restTemplate) {
        this.paymentService = paymentService;
        this.restTemplate = restTemplate;
    }

    @CircuitBreaker(
            name = "payment-option-service-circuit-breaker",
            fallbackMethod = "paymentOptionOnCircuitBreak"
    )
    public Map<String, List<PaymentOption>> availablePaymentOptions() {
        final Map<String, List<PaymentOption>> options = new HashMap<>(1);
        log.info("Payment service url: {}", paymentService);
        List<PaymentOption> paymentOptions = new ArrayList<>();
        final ResponseEntity<List<PaymentOption>> paymentOptionsResponseEntity = restTemplate
                .exchange(paymentService + "/payment", HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<PaymentOption>>() {
                        });
        if (paymentOptionsResponseEntity.getStatusCode() == HttpStatus.OK) {
            log.info("received payment options: {}", paymentOptionsResponseEntity.getBody());
            paymentOptions.addAll(paymentOptionsResponseEntity.getBody());
            paymentOptions.add(defaultPaymentOption());
        }
        options.put("payment_options", paymentOptions);
        return options;
    }

    private PaymentOption defaultPaymentOption() {
        return new PaymentOption("Cash On Delivery", PaymentOption.Type.COD);
    }

    public Map<String, List<PaymentOption>> paymentOptionOnCircuitBreak(Throwable throwable) {
        log.error("Error getPaymentweb-api {}", throwable.getMessage(), throwable);
        final Map<String, List<PaymentOption>> options = new HashMap<>(1);
        options.put("payment_options", Collections.singletonList(defaultPaymentOption()));
        return options;
    }
}
