package in.silentsudo.paymentoptions;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/payment")
public class PaymentOptionController {
    private final PaymentOptionsService paymentOptionsService;
    private static int number = 0;

    public PaymentOptionController(PaymentOptionsService paymentOptionsService) {
        this.paymentOptionsService = paymentOptionsService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PaymentOption> getPaymentOptions() throws Throwable {
        if (number++ > 10) {
            throw new IllegalStateException("Invalid getPayment state");
        }
        return paymentOptionsService.paymentOptions();
    }
}
