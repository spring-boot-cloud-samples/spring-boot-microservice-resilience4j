package in.silentsudo.webapi.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class DefaultConfiguration {

    public static final String STORE_SERVICE = "store-service";
    public static final String PAYMENT_SERVICE = "payment-option-service";

    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
//        return new RestTemplate();
        return new RestTemplateBuilder()
                .setReadTimeout(Duration.ofSeconds(30))
                .setConnectTimeout(Duration.ofSeconds(30))
                .build();

    }

    @Bean
    String storeService() {
        return "http://" + STORE_SERVICE;
    }

    @Bean
    String paymentService() {
        return "http://" + PAYMENT_SERVICE;
    }
}
