package in.silentsudo.storeservice.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/status")
public class StatusController {

    @GetMapping(value = "/shh", produces = MediaType.TEXT_PLAIN_VALUE)
    public String ping() {
        return "koi toh hai";
    }
}
