package in.silentsudo.paymentoptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class PaymentOption {
    @Getter
    private String name;

    @Getter
    private Type type;


    static enum Type {
        UPI,
        CARD
    }
}
